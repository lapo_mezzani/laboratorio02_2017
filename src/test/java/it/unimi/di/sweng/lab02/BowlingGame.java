package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private int[] rolls= new int[22];
	private int currentRoll=0;
	private int NUM_FRAMES=10;

	public void roll(int pins) {
		
		rolls[currentRoll++]=pins;
		
	}


	public int score() {

		int scores=0;
		int frame=0;
		currentRoll=0;
		
		for(frame=0; frame<NUM_FRAMES; frame++){ 
			
			if(currentRoll >= 1 && rolls[currentRoll-1]+rolls[currentRoll-2] == 10 && rolls[currentRoll-2] != 10) //spare
				rolls[currentRoll] *= 2;
			
				
			if(rolls[currentRoll] == 10 && frame != 9) {  //strike
				
				rolls[currentRoll+1] *= 2;
				rolls[currentRoll+2] *= 2;				
			  }
			
			
			if(frame == 9 && rolls[currentRoll] == 10) { //lastStrike
				
				scores += rolls[currentRoll]+(rolls[currentRoll+1]+rolls[currentRoll+2]);
				return scores;
			}
				

               scores += rolls[currentRoll]+rolls[currentRoll+1];

            	   currentRoll += 2;
               
		    }
		
		
		return scores;
		
	}

}
